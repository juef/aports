# Contributor: Patrycja Rosa <alpine@ptrcnull.me>
# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=exoscale
pkgver=1.77.0
pkgrel=1
pkgdesc="Command-line tool for everything at Exoscale"
url="https://github.com/exoscale/cli"
arch="all"
license="Apache-2.0"
makedepends="go"
source="https://github.com/exoscale/cli/archive/refs/tags/v$pkgver/exoscale-$pkgver.tar.gz"
builddir="$srcdir/cli-$pkgver"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -o exo -ldflags "-X main.version=$pkgver"
}

check() {
	go test github.com/exoscale/cli/cmd
}

package() {
	install -Dm755 exo -t "$pkgdir"/usr/bin
}

sha512sums="
fe21b9a2792046d4b64c21c3f5f5892f8550712c5c995d591d8e46592c2dd406b72100ddc172492ab7a142744cbefc67fda4f450901880eb5765d2d7a85a410b  exoscale-1.77.0.tar.gz
"
